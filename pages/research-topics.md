---
layout: page
permalink: "research-topics"
title: "Safety Analysis Group - GAS"
---

<div id="breadcrumb" style="width:980px; margin: 5px auto;">
	<span class="breadcrumbs pathway">Home > Research Topics</span>
</div>

<div class="clear"></div>
<div id="content">
	<div id="colmain" style="width:948px;">
		<div class="componentheading">
			Research Topics </div>
		<div id="article">
			<div class="buttonheading">
			</div>
			<p><span
					style="border-collapse: collapse; font-family: &#39;Lucida Grande&#39;, Verdana, Lucida, Helvetica, Arial, sans-serif; color: #000000; line-height: normal;">
				</span></p>
			<h1 class="documentFirstHeading"
				style="color: black; font-family: &#39;Lucida Grande&#39;, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0px; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 19px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
				<span style="font-size: 15px; font-weight: bold;">Research Topics</span></h1>
			<div id="parent-fieldname-text">
				<ul
					style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
					<li style="margin-bottom: 0.5em;">Methodologies for assessing the dependability levels (e.g. safety,
						reliability, availability, testability and maintainability) of critical systems;</li>
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">Methodologies and techniques for
							developing dependable systems (notably safety-critical systems);</span></li>
					<li style="margin-bottom: 0.5em;">Certification of embedded systems applied in real-time, high
						reliability and safety areas;</li>
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">New paradigms in air traffic
							management;</span></li>
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">New paradigms in intelligent
							transportation systems;</span></li>
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">Databases and data integrity in
							real-time and dependable systems;</span></li>
					<li style="margin-bottom: 0.5em;">Artificial intelligence applied to treat uncertainties in
						dependable systems;</li>
					<li style="margin-bottom: 0.5em;">Predictive maintenance of dependable systems.</li>
				</ul>
				<h1 class="documentFirstHeading"
					style="color: black; font-family: &#39;Lucida Grande&#39;, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0px; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 19px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
					<span style="font-size: 15px;font-weight: bold;">Technological Areas</span></h1>
				<ul
					style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
					<li style="margin-bottom: 0.5em;">Air traffic systems</li>
					<li style="margin-bottom: 0.5em;">Embedded systems</li>
					<li style="margin-bottom: 0.5em;">Power distribution systems</li>
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">Real-time systems</span></li>
					<li style="margin-bottom: 0.5em;">Railway and railroad systems</li>
					<li style="margin-bottom: 0.5em;">Unmanned vehicle systems - UVS (e.g. Unmanned aerial vehicles -
						UAV)</li>
					<li style="margin-bottom: 0.5em;">Autonomous cars and intelligent transportation systems</li>
				</ul>
			</div>
			<p>&nbsp;</p>
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="clear"></div>