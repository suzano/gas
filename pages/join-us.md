---
layout: page
permalink: "join-us"
title: "Safety Analysis Group - GAS"
---

<div id="breadcrumb" style="width:980px; margin: 5px auto;">
	<span class="breadcrumbs pathway">Home > Join-Us!</span>
</div>

<div class="clear"></div>
<div id="content">
	<div id="colmain" style="width:948px;">
		<div class="componentheading">
			Join Us! </div>
		<div id="article">
			<div class="buttonheading">
			</div>
			<div
				style="mso-element: para-border-div; border: none; border-bottom: solid #336699 1.0pt; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm 0cm 0cm 0cm;">
				<p class="MsoNormal"
					style="margin-bottom: 3.0pt; mso-line-height-alt: 11.85pt; mso-outline-level: 1; border: none; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm; mso-padding-alt: 0cm 0cm 0cm 0cm;">
					<span
						style="font-size: 14.5pt; font-family: Verdana,sans-serif; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; color: black; mso-font-kerning: 18.0pt; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Admissions</span>
				</p>
			</div>
			<p style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: normal;"><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">The Safety Analysis Group
					accepts undergraduate and graduate students interested in work on our </span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;"><a
						href="research-topics"
						target="_self"><strong><span lang="EN-US">research topics</span></strong></a></span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US"> and </span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;"><a
						href="projects"
						target="_self"><strong><span lang="EN-US">projects</span></strong></a></span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">.<br></span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">Keep an eye out for the
				</span><span style="font-size: 9pt; font-family: Tahoma, sans-serif;"><a
						href="news"
						target="_self"><strong><span lang="EN-US">News</span></strong></a></span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US"> section for
					opportunities!</span></p>
			<div
				style="mso-element: para-border-div; border: none; border-bottom: solid #336699 1.0pt; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm 0cm 0cm 0cm;">
				<p class="MsoNormal"
					style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; mso-line-height-alt: 11.85pt; mso-outline-level: 1; border: none; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm; mso-padding-alt: 0cm 0cm 0cm 0cm;">
					<span
						style="font-size: 14.5pt; font-family: Verdana,sans-serif; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; color: black; mso-font-kerning: 18.0pt; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Graduate Education</span>
				</p>
			</div>
			<p style="mso-margin-top-alt: auto; mso-margin-bottom-alt: auto; line-height: normal;"><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">Details about both Master's
					Degree (MSc) and Doctorate (PhD) programs are available in the </span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;"><a
						href="http://www.ppgee.poli.usp.br/"
						target="_blank"><strong><span lang="EN-US">Electrical Engineering Graduation Program Website
								(http://www.ppgee.poli.usp.br/</span></strong><span
							lang="EN-US">)</span></a></span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">. Admission examination occurs
					twice a year, usually in May and October.<br></span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">Feel free to send e-mails to
					the professors to talk about your topics of interest:</span></p>
			<ul type="disc">
				<li class="MsoNormal" style="line-height: normal;"><span
						style="font-size: 9.0pt; font-family: Tahoma,sans-serif; mso-fareast-font-family: Times New Roman; mso-fareast-language: PT-BR;">Prof.
						João Batista Camargo Júnior:&nbsp;
							<strong><a href="mailto:joaocamargo@usp.br">joaocamargo@usp.br</a>
							<script language="JavaScript" type="text/javascript">
								document.write('<span style=\'display: none;\'>');
							</script><span style="display: none;">This e-mail address is being protected from spambots. You need JavaScript
								enabled to
								view it
								<script language="JavaScript" type="text/javascript">
									document.write('</');
									document.write('span>');
								</script>
							</span>
						</strong></span></li>
			</ul>
			<ul type="disc">
				<li class="MsoNormal" style="line-height: normal;"><span
						style="font-size: 9.0pt; font-family: Tahoma,sans-serif; mso-fareast-font-family: Times New Roman; mso-fareast-language: PT-BR;">Prof.
						Jorge Rady de Almeida Júnior:&nbsp;<strong><a href="mailto:jorgerady@usp.br">jorgerady@usp.br</a>
							<script language="JavaScript" type="text/javascript">
								document.write('<span style=\'display: none;\'>');
							</script><span style="display: none;">This e-mail address is being protected from spambots. You need JavaScript
								enabled to
								view it
								<script language="JavaScript" type="text/javascript">
									document.write('</');
									document.write('span>');
								</script>
							</span>
						</strong></span></li>
			</ul>
			<ul type="disc">
				<li class="MsoNormal" style="line-height: normal;"><span
						style="font-size: 9.0pt; font-family: Tahoma,sans-serif; mso-fareast-font-family: Times New Roman; mso-fareast-language: PT-BR;">Prof.
						Paulo Sérgio Cugnasca:&nbsp;<strong><a href="mailto:cugnasca@usp.br">cugnasca@usp.br</a>
							<script language="JavaScript" type="text/javascript">
								document.write('<span style=\'display: none;\'>');
							</script><span style="display: none;">This e-mail address is being protected from spambots. You need JavaScript
								enabled to
								view it
								<script language="JavaScript" type="text/javascript">
									document.write('</');
									document.write('span>');
								</script>
							</span>
						</strong></span></li>
			</ul>
			<p>&nbsp;</p>
			<div
				style="mso-element: para-border-div; border: none; border-bottom: solid #336699 1.0pt; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm 0cm 0cm 0cm;">
				<p class="MsoNormal"
					style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; mso-line-height-alt: 11.85pt; mso-outline-level: 2; border: none; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm; mso-padding-alt: 0cm 0cm 0cm 0cm;">
					<span
						style="font-size: 13.5pt; font-family: Verdana,sans-serif; mso-fareast-font-family: Times New Roman; mso-bidi-font-family: Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Undergraduate Education</span>
				</p>
			</div>
			<p><span style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">We frequently have
					opportunities for young researchers willing to start working in our </span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;"><a
						href="projects"
						title="Research Projects"><span style="color: #123a67;" lang="EN-US">research
							projects</span></a></span><span style="font-size: 9pt; font-family: Tahoma, sans-serif;"
					lang="EN-US">. </span><span style="font-size: 9pt; font-family: Tahoma, sans-serif;">Send an e-mail
					to Prof. João Batista Camargo Jr. </span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">(</span><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;"><strong><a href="mailto:joaocamargo@usp.br">joaocamargo@usp.br</a>
						<script language="JavaScript" type="text/javascript">
							document.write('<span style=\'display: none;\'>');
						</script><span style="display: none;">This e-mail address is being protected from spambots. You need JavaScript
							enabled to
							view it
							<script language="JavaScript" type="text/javascript">
								document.write('</');
								document.write('span>');
							</script>
						</span>
					</strong></span><span style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">) if you
					are interested in joining the Safety Analysis Group.</span></p>
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="clear"></div>