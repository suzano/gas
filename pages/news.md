---
layout: page
permalink: "news"
title: "Safety Analysis Group - GAS"
---

<div id="breadcrumb" style="width:980px; margin: 5px auto;">
	<span class="breadcrumbs pathway">Home > News</span>
</div>

<div class="clear"></div>

<div id="content">
	<div id="colmain" style="width:948px;">
		<div class="componentheading">
			News </div>
		<div id="article">
			<div class="blog">
				<div class="leading">										
					<div class="article_row">

						{% for post in site.posts %}
						<div class="article_column column1 cols2">
							<div id="category_blogitem">
								<div>
									<h1 class="contentheading">
										<span>
											<a href="{{ post.url | prepend: site.baseurl}}"
												class="contentpagetitle">
												{{ post.title }}
												</a>
										</span>
									</h1>
									<div class="writenby">
									</div>
									<p><span
											style="border-collapse: collapse; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; font-weight: bold; line-height: 18px; color: #000000;">
											{{ post.description }}
											</span></p>
								</div>
							</div>
						</div>
						<span class="article_separator">&nbsp;</span>
						{% endfor %}
						
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="clear"></div>