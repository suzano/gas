---
layout: page
permalink: "people"
title: "Safety Analysis Group - GAS"
---

<div id="breadcrumb" style="width:980px; margin: 5px auto;">
	<span class="breadcrumbs pathway">Home > People</span>
</div>

<div class="clear"></div>

<div id="content">
	<div id="colmain" style="width:948px;">
		<div class="componentheading">
			People </div>
		<div id="article">
			<div class="buttonheading">
			</div>
			<p><span
					style="border-collapse: collapse; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; color: #000000; line-height: normal;">
				</span></p>
			<h2
				style="color: black; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0.75em; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 18px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
				Professors</h2>
			<ul
				style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
				<li style="margin-bottom: 0.5em;">João Batista Camargo Jr. - D.Eng Professor (Leader) -&nbsp;<a
						class="external-link"
						href="http://lattes.cnpq.br/7606805059403041"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Jorge Rady de Almeida Jr. - D.Eng Professor (Leader) -&nbsp;<a
						class="external-link"
						href="http://lattes.cnpq.br/9258926153708205"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Paulo Sérgio Cugnasca - D.Eng Professor -&nbsp;<a
						class="external-link"
						href="http://lattes.cnpq.br/9997641567631872"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Selma Shin Shimizu Melnikoff - D.Eng Professor -&nbsp;<a
						class="external-link"
						href="http://lattes.cnpq.br/7390536888473418"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
			</ul>
			<div>
				<h2
					style="color: black; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0.75em; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 18px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
					Post-Doctorate Students</h2>
				<ul
					style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">Carlos Enrique Hernández
							Simões</span><span style="line-height: 1.5em; font-size: 12.1599998474121px;"> - DSc. -
						</span><a
							href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4450885P5"
							target="_blank"
							style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
							CV]</a></li>
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">Jamil Kalil Naufal Jr. -
							D.Eng<span style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span>-
						</span><a class="external-link"
							href="http://lattes.cnpq.br/7725217117487636"
							style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
							CV]</a></li>
					<li style="margin-bottom: 0.5em;"><span
							style="line-height: 1.5em; font-size: 12.1599998474121px;">Ricardo Alexandre Veiga Gimenes -
							DSc. - </span><a class="external-link"
							href="http://lattes.cnpq.br/5074491194543167"
							style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
							CV]</a></li>
				</ul>
			</div>
			<h2
				style="color: black; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0.75em; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 18px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
				Doctorate (PhD) Students</h2>
			<ul
				style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
				<li style="margin-bottom: 0.5em;"><span style="line-height: 1.5em; font-size: 12.1599998474121px;">André
						Ippolito - M.Eng - </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4431159J1"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Bruno Zolotareff dos Santos - MSc<span
						style="line-height: 1.5em; font-size: 12.1599998474121px;"> - </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4701737H7"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Daniel Baraldi Sesso&nbsp;- MSc.&nbsp;<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4269733T6"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Derick Moreira Baum - M.Eng -<span
						style="font-size: 12.1599998474121px; line-height: 1.5em;"> </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4204621Y1"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;"><span
						style="line-height: 1.5em; font-size: 12.1599998474121px;">Flávio Monteiro Rachel - M.Eng -
					</span><a class="external-link"
						href="http://lattes.cnpq.br/1416917602141489"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">José Gomes Neto - MSc -&nbsp;<span
						style="line-height: 1.5em; font-size: 12.1599998474121px;"> </span><a class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4559592U5"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Julio Antonio do Amaral - MSc<span
						style="font-size: 12.16px; line-height: 18.24px;"> - </span><span
						style="line-height: 1.5em; font-size: 12.1599998474121px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4756202J2"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Lúcio Flávio Vismari - M.Eng -&nbsp;<a class="external-link"
						href="http://lattes.cnpq.br/3887659051159841"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Mauro Yuji Ohara - M.Eng -&nbsp;<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4596563P1"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Orlando da Silva Júnior - M.Eng -&nbsp;<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4357032H1"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Renan Buosi Ferreira - MSc.<span
						style="font-size: 12.16px; line-height: 18.24px;"> - </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4266710J3"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Rosa Helena Peccinini Silva Rossi - MSc. -<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4769101U5"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Sandra Kawamoto - M.Eng -<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4791988H4"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Thiago Toshio Matsumoto - MSc.&nbsp;<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4664653T5"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
			</ul>
			<h2
				style="color: black; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0.75em; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 18px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
				Master's Degree (MSc) Students</h2>
			<ul
				style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
				<li style="margin-bottom: 0.5em;">Caroline Bianca Santos Tancredi Molina<span
						style="font-size: 12.16px; line-height: 18.24px;"> - </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4307732D3"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Débora Ribeiro Doimo -<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4446814Y5"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Janaína Carli de Freitas<span
						style="font-size: 12.16px; line-height: 18.24px;"> -</span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4214053A2"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;"><span style="line-height: 1.5em; font-size: 12.16px;">João Gilberto
						Sanzovo - </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K8219412T3"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Laércio Quintanilha Fogaça Júnior -&nbsp;<a class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K8159863J6"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Paula Naomi Muniz dos Santos<span
						style="font-size: 12.16px; line-height: 18.24px;"> - </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K8386882A8"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Rodrigo Ignacio Rojas González</li>
				<li style="margin-bottom: 0.5em;">Sílvia Goldman Ber Kapel -&nbsp;<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K8145904D3"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Tharsis Martins Novais<span
						style="font-size: 12.16px; line-height: 18.24px;"> - </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4466361D9"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;"><span style="line-height: 1.5em; font-size: 12.16px;">Tiago Alves
						Silva</span></li>
				<li style="margin-bottom: 0.5em;">Tiago Martins de Alexandre<span
						style="font-size: 12.16px; line-height: 18.24px;"> - </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4458094U0"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Vinícius Maurício de Almeida&nbsp;<span
						style="font-size: 12.16px; line-height: 18.24px;">- </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4347355T3"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
				<li style="margin-bottom: 0.5em;">Wesley José Nogueira Medeiros -&nbsp;<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4447144H7"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a></li>
			</ul>
			<h2
				style="color: black; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0.75em; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 18px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
				Undergraduate Students</h2>
			<ul
				style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
				<li style="margin-bottom: 0.5em;">Amauri Shimabukuro</li>
				<li style="margin-bottom: 0.5em;">Leonardo Borges</li>
				<li style="margin-bottom: 0.5em;">Lucca Corsi</li>
				<li style="margin-bottom: 0.5em;">Pedro Darvas</li>
			</ul>
			<h2
				style="color: black; font-family: Lucida Grande, Verdana, Lucida, Helvetica, Arial, sans-serif; margin-top: 0.75em; margin-right: 0px; margin-bottom: 0.25em; margin-left: 0px; font-size: 18px; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #336699; font-weight: normal;">
				Contributors</h2>
			<ul
				style="line-height: 1.5em; list-style-image: url(http://sabia.pcs.usp.br/gas/bullet.gif); list-style-type: square; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0px; margin-left: 1.5em; padding: 0px;">
				<li style="margin-bottom: 0.5em;"><span
						style="line-height: 1.5em; font-size: 12.1599998474121px;">Antonio Vieira da Silva Neto - MSc. -
					</span><span
						style="line-height: 1.5em; font-size: 12.16px; color: #123a67; border-bottom: 1px solid #cccccc; padding: 1px 0px 1px 16px; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-color: transparent; background-origin: initial; background-clip: initial; background-position: 0px 1px; background-repeat: no-repeat; text-decoration: underline;"><a
							class="external-link"
							href="http://lattes.cnpq.br/0532613714699664"
							style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
							CV]</a></span><span style="font-size: 12.16px; line-height: 18.24px;"> (FDTE)</span></li>
				<li style="margin-bottom: 0.5em;"><span
						style="line-height: 1.5em; font-size: 12.1599998474121px;">Bemildo Álvaro Ferreira Filho
						(INFRAERO)</span></li>
				<li style="margin-bottom: 0.5em;">Benício José de Souza - D.Eng Professor (retired) -<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;"> </span><a
						class="external-link"
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4760672D1"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a> (Poli-USP)</li>
				<li style="margin-bottom: 0.5em;">Carlos Henrique Netto Lahoz - D.Eng -&nbsp;<a
						href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4720526J0"
						target="_blank"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a> <span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;">(IAE/CTA)</span></li>
				<li style="margin-bottom: 0.5em;">Eno Siewerdt -&nbsp;<a class="external-link"
						href="http://lattes.cnpq.br/7608076474310803"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a><span style="font-size: 12.16px;"> </span><span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;">(Atech/Embraer)</span>
				</li>
				<li style="margin-bottom: 0.5em;">Italo Romani de Oliveira - D.Eng -&nbsp;<a class="external-link"
						href="http://lattes.cnpq.br/5101099283202921"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a> (Boeing Research &amp; Technology)</li>
				<li style="margin-bottom: 0.5em;"><span style="line-height: 1.5em; font-size: 12.16px;">Marcelo José Ruv
						Lemes - D.Eng - </span><a class="external-link"
						href="http://lattes.cnpq.br/6943467443046329"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a><span style="line-height: 1.5em; font-size: 12.16px;"> (Embraer)</span></li>
				<li style="margin-bottom: 0.5em;">Paulo Antonio Mariotto - D.Eng Professor (retired) (Poli-USP)</li>
				<li style="margin-bottom: 0.5em;">Ricardo Caneloi dos Santos - D.Eng&nbsp;<span
						style="font-size: 12.1599998474121px; line-height: 18.2399997711182px;">Professor </span>
					-&nbsp;<a class="external-link"
						href="http://lattes.cnpq.br/1277989540271729"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a> (UFABC)</li>
				<li style="margin-bottom: 0.5em;">Valter Fernandes Avelino - D.Eng Professor -&nbsp;<a
						class="external-link"
						href="http://lattes.cnpq.br/8003431098276221"
						style="color: #123a67; background-color: transparent; text-decoration: none; border-bottom-width: 1px; border-bottom-color: #cccccc; border-bottom-style: solid; background-image: url(http://sabia.pcs.usp.br/gas/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; background-position: 0px 1px; background-repeat: no-repeat no-repeat;">[Lattes
						CV]</a><span style="line-height: 1.5em; font-size: 12.1599998474121px;"> (FEI)</span></li>
			</ul>
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="clear"></div>