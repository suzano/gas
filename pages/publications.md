---
layout: page
permalink: "publications"
title: "Safety Analysis Group - GAS"
---

<div id="breadcrumb" style="width:980px; margin: 5px auto;">
	<span class="breadcrumbs pathway">Home > Publications</span>
</div>

<div class="clear"></div>

<div id="content">
	<div id="colmain" style="width:948px;">
		<div class="componentheading">
			Publications </div>
		<div id="article">
			<div class="buttonheading">
			</div>
			<h1>Members' publications</h1>
			<p>The updated list of publications by the Safety Analysis Group members can be found in the <em>Lattes
					CVs</em> of the researchers (under the section '<em>Produções</em>'). The links to the <em>Lattes
					CVs</em> are available in the <a
					href="people"
					target="_self"><span style="text-decoration: underline;"><strong>People</strong></span></a> section.
			</p>
			<h1><span class="caption">JBATS</span></h1>
			<p>Papers published on the <strong>Journal of the Brazilian Air Transportation Research Society</strong>
				<strong>- JBATS</strong> can be accessed from the JBATS <span style="text-decoration: underline;"><a
						class="caption"
						href="/jbats"
						target="_blank" title="JBATS">site</a></span>.
			</p>
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="clear"></div>