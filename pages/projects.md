---
layout: page
permalink: "projects"
title: "Safety Analysis Group - GAS"
---

<div id="breadcrumb" style="width:980px; margin: 5px auto;">
	<span class="breadcrumbs pathway">Home > Projects</span>
</div>

<div class="clear"></div>

<div id="content">
	<div id="colmain" style="width:948px;">
		<div class="componentheading">
			Projects </div>
		<div id="article">
			<div class="buttonheading">
			</div>
			<div
				style="mso-element: para-border-div; border: none; border-bottom: solid #336699 1.0pt; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm 0cm 0cm 0cm;">
				<p class="MsoNormal"
					style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; mso-line-height-alt: 11.85pt; mso-outline-level: 2; border: none; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm; mso-padding-alt: 0cm 0cm 0cm 0cm;">
					<span
						style="font-size: 13.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Current Projects</span>
				</p>
			</div>
			<p class="MsoNormal"
				style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; line-height: 11.85pt; mso-outline-level: 3;">
				<strong><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Safety Analysis of São Paulo Metro Signalling and Control Systems
						(METRÔ-SP)</span></strong>
			</p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><strong><span
						style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">1981-current</span></strong><span
					style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
					lang="EN-US"> </span></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">The Safety Analysis Group has
					performed several extension and research projects related to the safety analysis of signalling and
					control systems of São Paulo Metro, responsible for the transportation of over 3 million people
					every day in São Paulo city. These projects consist on the use of safety analysis methodologies and
					techniques to analyze the safety levels of signalling and control systems, including qualitative and
					quantitative analyses of systems specification and design (e.g. hardware analyses and software
					analyses).</span></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">Additionally, the Safety
					Analysis Group has also worked with metro transit systems of other Brazilian cities, such as Belo
					Horizonte, Porto Alegre, Recife and Fortaleza.</span></p>
			<p class="MsoNormal"
				style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; line-height: 11.85pt; mso-outline-level: 3;">
				<strong><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Safety and Resilience in Automotive Cyber-Physical Systems (partnership with
						Ericsson Research - ER)</span></strong>
			</p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><strong><span
						style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">2015-current</span></strong></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span class="caption"
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">The main goal of this research
					project is to develop a framework to safety and resilience analysis for the future automotive
					cyber-physical critical systems, mainly over the Intelligent Transportation Systems (ITS) concept.
					This analysis framework intends to be based on fast-time and real-time computer simulations, where
					Model-In-the-Loop (MIL), Hardware-In-The-Loop (HIL) and Human-In-The-Loop approaches could be
					considered. Using this framework allows to analyze, in a broad sense, the impacts of concepts,
					technologies and procedures (including standards and regulations) on automotive cyber physical
					systems resilience properties: availability, reliability, safety, security and maintainability when
					systems facing changes</span>.</p>
			<p class="MsoNormal"
				style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; line-height: 11.85pt; mso-outline-level: 3;">
				<strong style="font-size: 12.16px; line-height: 15.8px;"><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Safety in Aviation Technologies and ATM (partnership with Boeing Research &amp;
						Techonlogy - BR&amp;T)</span></strong>
			</p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><strong><span
						style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">2015-current</span></strong></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span
					style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;">The main objectives of
					this research project are&nbsp;(1)&nbsp;Presenting the </span><span
					style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;">state of the
					art</span><span style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;"> of
					GAS/POLI-USP researches, as well as a </span><span
					style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;">detailed research
					proposal and a Roadmap</span><span
					style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;"> for future potential
					research projects, related to the projects in the areas of research opportunity in Air Traffic
					Management (ATM) identified by BR&amp;T in Brazil; and&nbsp;(2)&nbsp;Carrying out the first three
					long term research projects&nbsp;related to the projects in the areas of research opportunity in Air
					Traffic Management (ATM) identified by BR&amp;T in Brazil: </span><span
					style="font-size: 12.16px; line-height: 1.3em; text-indent: -18pt;">Human Factors and ATM
					Efficiency</span><span style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;">,
				</span><span style="font-size: 12.16px; line-height: 1.3em; text-indent: -18pt;">Safety Analysis of
					Complex Systems/Priority and Maneuvering in Airspace</span><span
					style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;"> </span><span
					style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;">and </span><span
					style="font-size: 12.16px; line-height: 1.3em; text-indent: -18pt;">Ionosphere Correction Algorithm
					for GPS Applications</span><span
					style="font-family: Tahoma, sans-serif; font-size: 9pt; line-height: 18pt;">.&nbsp;Three main
					results are expected on this project: (i) the demonstration of technical competence to carry out a
					set of projects from those potential projects presented by BR&amp;T; (ii) a detailed, long term
					research proposal (including a roadmap) for carrying out the set of potential projects presented by
					BR&amp;T in which GAS/POLI-USP has competence and expertize; and&nbsp;(iii) the execution of the
					three aforementioned long term research projects.</span></p>
			<p class="MsoNormal"
				style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; line-height: 11.85pt; mso-outline-level: 3;">
				<strong><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Integrated Platform for Testing Critical Embedded Systems
						(PIpE-SEC)</span></strong>
			</p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><strong><span
						style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">2009-current</span></strong></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span class="caption"
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">The Integrated Platform for
					Testing Critical Embedded Systems (PIpE-SEC) is a test tool under development by the Safety Analysis
					Group. Its development started during the INCT-SEC project (2009-2013). It allows modeling and
					real-time simulations of air traffic operations, covering the actual structural and behavioral
					characteristics of the Air Traffic Control System (ATC), including the interactions among air
					traffic controllers (ATCos) and aircraft, be they manned or computer-piloted vehicles. In this
					real-time simulated environment, controlled tests make it possible to evaluate and validate the
					concepts intrinsic to the project of both manned and unmanned vehicles (especially those that are
					autonomous) and their interaction with the operational environment in which they are inserted,
					pondering the characteristics related to the technologies and to the procedures applied.</span></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt; mso-outline-level: 4;"><strong><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US"> </span></strong></p>
			<div
				style="mso-element: para-border-div; border: none; border-bottom: solid #336699 1.0pt; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm 0cm 0cm 0cm;">
				<p class="MsoNormal"
					style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; mso-line-height-alt: 11.85pt; mso-outline-level: 2; border: none; mso-border-bottom-alt: solid #336699 .75pt; padding: 0cm; mso-padding-alt: 0cm 0cm 0cm 0cm;">
					<span
						style="font-size: 13.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Past Projects</span>
				</p>
			</div>
			<p class="MsoNormal"
				style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; line-height: 11.85pt; mso-outline-level: 3;">
				<strong><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">National Institute of Science and Technology for Critical Embedded Systems
						(INCT-SEC)</span></strong>
			</p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><strong><span
						style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">2009-2013</span></strong></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">The INCT-SEC (Instituto
					Nacional de Ciências e Tecnologia em Sistemas Embarcados Críticos) was created to improve the
					national knowledge about critical embedded&nbsp; systems. These systems are applied in strategic
					areas like environment, security, safety, defense and agriculture. One of the main objectives was to
					develop autonomous vehicles prototypes. The project, which was funded by the National Council for
					Scientific and Technological Development (CNPq), involved not only research groups from many
					universities, but also business companies.</span></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt; mso-outline-level: 4;"><strong><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">Critical Systems Laboratory for Transport Systems Certification</span></strong></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><strong><span
						style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">2009 - 2010</span></strong></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">This project aimed to deploy a
					computer infrastructure to support research in certification of air and railway transport systems.
					This project was funded by National Council for Scientific and Technological Development
					(CNPq).</span></p>
			<p class="MsoNormal"
				style="margin-top: 9.0pt; margin-right: 0cm; margin-bottom: 3.0pt; margin-left: 0cm; line-height: 11.85pt; mso-outline-level: 3;">
				<strong><span
						style="font-size: 11.5pt; font-family:Verdana,sans-serif; mso-fareast-font-family:Times New Roman; mso-bidi-font-family:Times New Roman; color: black; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">GIGA Project</span></strong>
			</p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><strong><span
						style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
						lang="EN-US">2004-2007</span></strong><span
					style="font-size: 8.5pt; font-family:Tahoma,sans-serif; mso-fareast-font-family:Times New Roman; color: #76797c; mso-ansi-language: EN-US; mso-fareast-language: PT-BR;"
					lang="EN-US"> </span></p>
			<p class="MsoNormal" style="margin-bottom: 9.0pt; line-height: 18.0pt;"><span
					style="font-size: 9pt; font-family: Tahoma, sans-serif;" lang="EN-US">The GIGA Project was carried
					out by many research groups funded by RNP-FINEP (National Research Network) to develop a CDM
					(collaborative decision making) system prototype. This prototype was used to evaluate CDM in air
					traffic management.</span></p>
			<p class="MsoNormal">&nbsp;</p>
		</div>
		<div class="clear"></div>
	</div>
</div>

<div class="clear"></div>